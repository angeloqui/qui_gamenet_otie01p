﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Photon.Pun;

public class TakeDamage : UnityEvent<Shooting, string> {}

public class Shooting : MonoBehaviourPunCallbacks {

    public TakeDamage Evt_TakeDamage = new TakeDamage();
    public TakeDamage Evt_OnEnemyWin = new TakeDamage();

    public GameObject hitEffectPrefab;


    /* REFERENCES */
    [HideInInspector] public GameManager GameManager;
    [SerializeField] Camera camera;

    /* STATS */
    [Header("HP Related Stuff")]
    public float startHealth = 100;
    float health;
    public Image HealthBar;
    bool isDead;

    [Header("Scoring")]
    public int kills;
    [HideInInspector] public Text killCountText;
    private int damage = 25;

    void Start() {
        health = startHealth;
    }

    public void Fire() {

        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200)) {

            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine) {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info) {
        health -= damage;
        HealthBar.fillAmount = health / startHealth;

        if (health <= 0 && !isDead) {

            isDead = true;
            
            /* WIN LOGIC */

            GameObject clone = info.Sender.TagObject as GameObject;

            if (!photonView.IsMine) {
                Shooting shooter = clone.GetComponent<Shooting>();

                shooter.kills++;
                shooter.killCountText.text = "KILLS: " + shooter.kills;

                if (shooter.kills >= 10) {
                    photonView.RPC("DispayGameOver", RpcTarget.All, info.Sender.NickName);
                }
                //Evt_OnEnemyWin.Invoke(this, info.Sender.NickName + " WON!");
            }

            
            /* END WIN LOGIC */

            Evt_TakeDamage.Invoke(this, info.Sender.NickName + " - (X) " + info.photonView.Owner.NickName);

            Die();  
        }
    }

    [PunRPC]
    public void DispayGameOver(string text) {
        Text gameOverText = GameObject.Find("GameOverText").GetComponent<Text>();
        gameOverText.text = text + " WON!";
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position) {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die() {
        if (photonView.IsMine) {
            GameManager.Player.Animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown() {
        
        GameObject respawnText = GameObject.Find("Respawn Text");

        float respawnTime = 5.0f;

        while (respawnTime > 0) {
            
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are killed. Respawning in " + respawnTime.ToString(".00");
        }

        respawnText.GetComponent<Text>().text = "";
        
        transform.position = GameManager.RespawnAreas[Random.Range(0, GameManager.RespawnAreas.Length)].position;

        GameManager.Player.Animator.SetBool("isDead", false);
        GameManager.Player.PlayerMovementController.enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void RegainHealth() {
        health = 100;
        HealthBar.fillAmount = health / startHealth;

        isDead = false;
    }
}
