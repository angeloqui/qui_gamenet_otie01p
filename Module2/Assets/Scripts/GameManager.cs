﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class GameManager : MonoBehaviourPunCallbacks {

    public GameObject playerPrefab;

    public PlayerSetup Player { get; private set; }
    public List<GameObject> PlayerList;

    public Transform[] RespawnAreas;

    void Start() {
        
        if (PhotonNetwork.IsConnectedAndReady) {
            
            //Get reference to all previously created players when first joining the room
            GetAllPlayerObject();

            //Create the player's unit
            InstantiatePlayer();
        }
    }

    void GetAllPlayerObject() {
        GameObject[] Players = GameObject.FindGameObjectsWithTag("Player");
        
        foreach (GameObject player in Players) {
            PlayerList.Add(player);
        }
    }

    void InstantiatePlayer() {
        GameObject clone = PhotonNetwork.Instantiate(playerPrefab.name, RespawnAreas[Random.Range(0, RespawnAreas.Length)].position, Quaternion.identity);
        
        Player = clone.GetComponent<PlayerSetup>();
        Player.PhotonView.Owner.TagObject = clone;
        Player.GameManager = this;

        PlayerList.Add(clone);
        AddListeners();
    }


    [Header("HUD")]
    [SerializeField] GameObject Canvas;

    [SerializeField] GameObject Scoreboard;
    [SerializeField] GameObject KillPanelPrefab;

    [SerializeField] Text GameOverText;

    [PunRPC]
    public void UpdateKillBoard(string text) {
            
        GameObject clone = Instantiate(KillPanelPrefab, Vector2.zero, Quaternion.identity);
        clone.transform.parent = Scoreboard.transform;

        clone.transform.Find("Text").GetComponent<Text>().text = text;

        Destroy(clone, 5.0f);
    }

    [PunRPC]
    public void GameOver(string text) {
        Debug.Log("game over");
        GameOverText.text = text;
    }

    //Events
    public void AddListeners() {

        Player.Shooting.Evt_TakeDamage.AddListener((Shooting, text) => {
            photonView.RPC("UpdateKillBoard", RpcTarget.AllBuffered, text);
        });

        Player.Shooting.Evt_OnEnemyWin.AddListener((Shooting, text) => {
            Debug.Log("called listener");
            photonView.RPC("GameOver", RpcTarget.AllBuffered, text);
        });
    }

}