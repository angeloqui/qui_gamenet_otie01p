﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerSetup : MonoBehaviourPunCallbacks {

    [HideInInspector] public GameManager GameManager;

    public PhotonView PhotonView { get; private set; }
    public RigidbodyFirstPersonController RigidbodyFirstPersonController { get; private set; }
    public PlayerMovementController PlayerMovementController { get; private set; }
    public Animator Animator { get; private set; }

    public Shooting Shooting { get; private set; }

    void Awake() {

        PhotonView = GetComponent<PhotonView>();
        RigidbodyFirstPersonController = GetComponent<RigidbodyFirstPersonController>();
        PlayerMovementController = GetComponent<PlayerMovementController>();
        Animator = GetComponent<Animator>();
        
        Shooting = GetComponent<Shooting>();
    }

    public GameObject fpsModel;
    public GameObject nonFpsModel;

    public GameObject playerUiPrefab;

    public Camera fpsCamera;

    public Avatar fpsAvatar, nonFpsAvatar;

    public GameObject playerUi { get; private set; }

    void Start() {

        Shooting.GameManager = GameManager;

        Animator.SetBool("isLocalPlayer", photonView.IsMine);
        Animator.avatar = photonView.IsMine ? fpsAvatar : nonFpsAvatar;

        fpsModel.SetActive(photonView.IsMine);
        nonFpsModel.SetActive(!photonView.IsMine);

        if (photonView.IsMine) {
            
            HUDSetup();

            fpsCamera.enabled = true;
        
        } else {

            PlayerMovementController.enabled = false;
            RigidbodyFirstPersonController.enabled = false;
            fpsCamera.enabled = false;
        }
    }

    [Header("UI")]
    [SerializeField] Text NameTag;

    void HUDSetup() {

        playerUi = Instantiate(playerUiPrefab);

        PlayerMovementController.fixedTouchField = playerUi.transform.Find("RotationTouchField").GetComponent<FixedTouchField>();
        PlayerMovementController.joystick = playerUi.transform.Find("Fixed Joystick").GetComponent<Joystick>();
        
        playerUi.transform.Find("FireButton").GetComponent<Button>().onClick.AddListener(() => Shooting.Fire());

        Shooting.killCountText = playerUi.transform.Find("Score Text").GetComponent<Text>();

        //Update through Servers
        photonView.RPC("UpdateNameTag", RpcTarget.AllBuffered);
    }

    [PunRPC]
    void UpdateNameTag() {
        NameTag.text = PhotonView.Owner.NickName;
    }
}
