﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySetup : MonoBehaviour {

    public PlayerSetup Player { get; private set; }

    public EnemyHealth Health { get; private set; }
    public EnemyMovement Movement { get; private set; }
    public EnemyAttack Attack { get; private set; }

    NavMeshAgent nav;

    //List of Players
    GameObject[] PlayerObjects;

    void Awake () {

        Health = GetComponent<EnemyHealth>();
        Movement = GetComponent<EnemyMovement>();
        Attack = GetComponent<EnemyAttack>();

        nav = GetComponent<NavMeshAgent>();

        FindPlayer();
        AddListeners();
    }

    public void FindPlayer() {

        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerSetup>();

        InitializeComponents();
    }

    void InitializeComponents() {
        Movement.player = Player.gameObject.transform;
        Movement.playerHealth = Player.Health;

        Attack.player = Player.gameObject;
        Attack.playerHealth = Player.Health;
    }

    void AddListeners() {
        
        Health.Evt_Death.AddListener((Health) => {
            
            Movement.enabled = false;
            Attack.enabled = false;
            nav.enabled = false;
        });
    }
}
