﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class GameManager : MonoBehaviourPunCallbacks {
    
    public GameObject[] vehiclePrefabs;
    public Transform[] startingPositions;

    public GameObject[] finisherTextUi;
    
    public static GameManager instance = null;

    public Text timeText;

    public List<Camera> Cameras = new List<Camera>();

    [SerializeField] GameObject Scoreboard;
    [SerializeField] GameObject KillPanelPrefab;

    public PlayerSetup Player { get; private set; }

    [SerializeField] Text SurviveTimer;

    void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start() {
        
        if (PhotonNetwork.IsConnectedAndReady) {

            object playerSelectionNumber;

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber)) {

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;

                Player = PhotonNetwork.Instantiate(vehiclePrefabs[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity).GetComponent<PlayerSetup>();
                Player.SurviveText = SurviveTimer;
            }
        }

        foreach (GameObject go in finisherTextUi) {
            go.SetActive(false);
        }
    }
}