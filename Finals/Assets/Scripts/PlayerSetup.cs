﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks {

    public string Name { get; private set; }

    [SerializeField] Camera camera;

    public Camera Camera {
        get { return camera; }
    }

    public PhotonView PhotonView { get; private set; }

    public PlayerHealth Health { get; private set; }
    public PlayerMovement Movement { get; private set; }

    [SerializeField] PlayerShooting shooting;

    public PlayerShooting Shooting {
        get { return shooting; }
    }

    public CountdownManager CountdownManager { get; private set; }

    bool isAlive = true;
    int score;
    float timer;

    [Header("UI")]
    [SerializeField] Text NameTag;
    [HideInInspector] public Text SurviveText;

    void Awake() {

        PhotonView = GetComponent<PhotonView>();

        Health = GetComponent<PlayerHealth>();
        Movement = GetComponent<PlayerMovement>();

        CountdownManager = GetComponent<CountdownManager>();
        CountdownManager.timerText = GameManager.instance.timeText;

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("sg")) {

            Shooting.enabled = false;
        }

        AddListeners();
        InitializeMultiplayer();
    }

    void Update() {

        if (isAlive) {
            timer += Time.deltaTime;
            SurviveText.text = "Surived for \n" + timer.ToString("F2") + "s \nScore : " + score;
        }
    }

    void InitializeMultiplayer() {

        Name = PhotonView.Owner.NickName;

        Camera.enabled = photonView.IsMine;
        Movement.enabled = photonView.IsMine;
        Shooting.enabled = photonView.IsMine;

        if (PhotonView.IsMine) {

            Camera.transform.parent = null;
        }

        photonView.RPC("UpdateNameTag", RpcTarget.AllBuffered);
    }

    void AddListeners() {

        Health.Evt_Death.AddListener((Health) => {
            
            Movement.enabled = false;
            Shooting.enabled = false;

            Shooting.DisableEffects();
        });
    }

    public void SpecatorMode() {
        
        isAlive = false;

    }

    public void AddScore(int value) {
        
        if (!isAlive) return;

        score += value;
    }

    [PunRPC]
    void UpdateNameTag() {
        NameTag.text = Name;
        NameTag.gameObject.SetActive(!photonView.IsMine);
    }
}
