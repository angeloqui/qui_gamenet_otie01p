﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public Shooting shooter;

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Player") && shooter != other.GetComponent<Shooting>()) {
            shooter.OnProjectileHit(other.gameObject);
        }
    }
}
