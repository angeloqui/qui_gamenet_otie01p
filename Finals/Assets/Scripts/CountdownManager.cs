﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class CountdownManager : MonoBehaviourPunCallbacks {

    public Text timerText;
    public float timeToStartRace = 3.0f;

    void Update() {

        if (!PhotonNetwork.IsMasterClient) return;

        if (timeToStartRace > 0) {
            timeToStartRace -= Time.deltaTime;
            photonView.RPC("SetTime", RpcTarget.AllBuffered, timeToStartRace);
        } else if (timeToStartRace < 0) {
            photonView.RPC("StartRace", RpcTarget.AllBuffered);
        }
    }

    [PunRPC]
    public void SetTime(float time) {
        if (time > 0) {
            timerText.text = time.ToString("F1");
        } else {
            timerText.text = "";
        }
    }

    [PunRPC]
    public void StartRace() {

        PlayerSetup setup = GetComponent<PlayerSetup>();

        setup.Movement.isControlEnabled = true;
        setup.Shooting.enabled = photonView.IsMine;
        
        this.enabled = false;
    }
}
