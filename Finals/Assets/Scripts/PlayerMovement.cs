﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    int floorMask;

    float speed = 4;
    float camRayLength = 100f; 

    public bool isControlEnabled;

    Vector3 movement;

    [HideInInspector] public Camera camera;

    Rigidbody rigidbody;
    Animator animator;

    void Awake() {
        floorMask = LayerMask.GetMask ("Floor");

        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    void FixedUpdate() {

        if (!isControlEnabled) return;

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Move(h, v);

        Turning();

        Animating (h, v);
    }
    
    void Move (float h, float v) {
        
        movement.Set (h, 0f, v);
            
        movement = movement.normalized * speed * Time.deltaTime;    
        
        rigidbody.MovePosition(transform.position + movement);
    }

    void Turning () {
        
        Ray camRay = camera.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        if (Physics.Raycast (camRay, out floorHit, camRayLength, floorMask)) {

            Vector3 playerToMouse = floorHit.point - transform.position;

            playerToMouse.y = 0f;

            Quaternion newRotatation = Quaternion.LookRotation (playerToMouse);

            rigidbody.MoveRotation(newRotatation);
        }
    }

    void Animating (float h, float v) {

        bool walking = h != 0f || v != 0f;

        animator.SetBool ("IsWalking", walking);
    }
}