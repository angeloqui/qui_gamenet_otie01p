﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;
using UnityEngine.Events;

public class EnemyMovement : MonoBehaviour {
        
    public float visionRange = 10f;
    public float hearingRange = 20f;
    public float wanderDistance = 10f;
    public Vector2 idleTimeRange;
    [Range(0f,1f)]
    public float psychicLevels = 0.2f;

    float currentVision; 
    NavMeshAgent nav;
    public float timer = 0f;

    [HideInInspector] public Transform player;
    [HideInInspector] public PlayerHealth playerHealth;

    void Awake () {
        nav = GetComponent<NavMeshAgent>();
    }

    void OnEnable() {
        nav.enabled = true;
        ClearPath();
        ScaleVision(1f);
        IsPsychic();
        timer = 0f;
    }
    
    void ClearPath() {
        if (nav.hasPath)
            nav.ResetPath();
    }

    void Update () {
        if (playerHealth.CurrentHealth > 0) {
            LookForPlayer();
            WanderOrIdle();
        }
        else {
            nav.enabled = false;
        }
    }

    void OnDestroy() {

        nav.enabled = false;
    }

    void LookForPlayer() {

        TestSense(player.position, currentVision);
    }

    void HearPoint(Vector3 position) {

        TestSense(position, hearingRange);
    }

    void TestSense(Vector3 position, float senseRange) {
            
        if (Vector3.Distance(this.transform.position, position) <= senseRange) {
                
            GoToPosition(position);
        }
    }

    public void GoToPlayer() {

        GoToPosition(player.position);
    }

    void GoToPosition(Vector3 position) {

        timer = -1f;

        SetDestination(position);
    }

    void SetDestination(Vector3 position) {
            
        if (nav.isOnNavMesh) {
                
            nav.SetDestination(position);
        }
    }

    private void WanderOrIdle() {

        if (!nav.hasPath) {

            if (timer <= 0f) {

                SetDestination(GetRandomPoint(wanderDistance, 5));
                    
                if (nav.pathStatus == NavMeshPathStatus.PathInvalid) {

                    ClearPath();
                }

                timer = Random.Range(idleTimeRange.x, idleTimeRange.y);
            }
            else {

                timer -= Time.deltaTime;
            }
        }
    }

    void IsPsychic() {

        GoToPlayer();
    }

    Vector3 GetRandomPoint(float distance, int layermask) {

        Vector3 randomPoint = UnityEngine.Random.insideUnitSphere * distance + this.transform.position;;

        NavMeshHit navHit;
        NavMesh.SamplePosition(randomPoint, out navHit, distance, layermask);

        return navHit.position;
    }

    public void ScaleVision(float scale) {

        currentVision = visionRange * scale;
    }

    int GetCurrentNavArea() {

        NavMeshHit navHit;
        nav.SamplePathPosition(-1, 0.0f, out navHit);

        return navHit.mask;
    }
}