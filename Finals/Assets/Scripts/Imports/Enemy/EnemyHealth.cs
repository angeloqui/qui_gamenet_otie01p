﻿using UnityEngine;
using UnityEngine.Events;

using Photon.Pun;

public class EnemyDeath : UnityEvent<EnemyHealth> {}

public class EnemyHealth : MonoBehaviour {

    public EnemyDeath Evt_Death = new EnemyDeath();

    public int startingHealth = 100;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;
    public AudioClip deathClip;

    int currentHealth;
    Animator anim;
    AudioSource enemyAudio;
    ParticleSystem hitParticles;
    CapsuleCollider capsuleCollider;
    EnemyMovement enemyMovement;

    void Awake ()
    {
        anim = GetComponent <Animator> ();
        enemyAudio = GetComponent <AudioSource> ();
        hitParticles = GetComponentInChildren <ParticleSystem> ();
        capsuleCollider = GetComponent <CapsuleCollider> ();
        enemyMovement = this.GetComponent<EnemyMovement>();
    }

    void OnEnable() {
        currentHealth = startingHealth;
        SetKinematics(false);
    }

    private void SetKinematics(bool isKinematic)
    {
        capsuleCollider.isTrigger = isKinematic;
        capsuleCollider.attachedRigidbody.isKinematic = isKinematic;
    }

    void Update ()
    {
        if (IsDead())
        {
            transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
            if (transform.position.y < -10f)
            {
                Destroy(this.gameObject);
            }
        }
    }

    public bool IsDead()
    {
        return (currentHealth <= 0f);
    }

    [PunRPC]
    public void TakeDamage (int amount, Vector3 hitPoint)
    {
        if (!IsDead())
        {
            enemyAudio.Play();
            currentHealth -= amount;

            if (IsDead())
            {
                Death();        
            }
            else
            {
                enemyMovement.GoToPlayer();
            }
        }
            
        hitParticles.transform.position = hitPoint;
        hitParticles.Play();
    }

    void Death ()
    {
        //EventManager.TriggerEvent("Sound", this.transform.position);
        anim.SetTrigger ("Dead");

        enemyAudio.clip = deathClip;
        enemyAudio.Play ();

        GameObject[] playerObjects = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject player in playerObjects) {
            player.GetComponent<PlayerSetup>().AddScore(scoreValue);
        }

        //Evt_Death.Invoke(this);

        //Debug.Log("Invoked!");
    }

    public void StartSinking ()
    {
        GetComponent <UnityEngine.AI.NavMeshAgent> ().enabled = false;
        SetKinematics(true);
    }

    public int CurrentHealth()
    {
        return currentHealth;
    }

    #region Network

    public void RPCTakeDamage(int damage, Vector3 hitPoint) {
        GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage, hitPoint);
    }

    #endregion
}