﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using Photon.Pun;

public class EnemyAttack : MonoBehaviour {

    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;

    Animator anim;
    [HideInInspector] public GameObject player;
    [HideInInspector] public PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    bool playerInRange;
    float timer;

    void Awake () {
        enemyHealth = GetComponent<EnemyHealth>();
        anim = GetComponent <Animator> ();
    }

    void OnTriggerEnter (Collider other)
    {
        // If the entering collider is the player...
        if(other.gameObject == player)
        {
            // ... the player is in range.
            playerInRange = true;
        }
    }

    void OnTriggerExit (Collider other)
    {
        // If the exiting collider is the player...
        if(other.gameObject == player)
        {
            // ... the player is no longer in range.
            playerInRange = false;
        }
    }

    void Update () {
        
        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;

        // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
        if(timer >= timeBetweenAttacks && playerInRange && enemyHealth.CurrentHealth() > 0)
        {
            // ... attack.
            Attack ();
        }

        // If the player has zero or less health...
        if (playerHealth.CurrentHealth <= 0) {

            anim.SetTrigger ("PlayerDead");

            OnPlayerDies();
        }
    }

    void OnPlayerDies() {

        playerInRange = false;
                    
        playerHealth.RPCHidePlayer();
        GetComponent<EnemySetup>().FindPlayer();

    }

    void Attack ()
    {
        // Reset the timer.
        timer = 0f;

        // If the player has health to lose...
        if(playerHealth.CurrentHealth > 0)
        {
            // ... damage the player.
            playerHealth.RPCTakeDamage(attackDamage);
        }
    }
}