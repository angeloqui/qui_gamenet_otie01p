﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class PlayerDeath : UnityEvent<PlayerHealth> {}

public class PlayerHealth : MonoBehaviourPunCallbacks {

    public enum RaiseEventsCode {
        DiedEventCode = 0
    }

    private int deathOrder = 0;

    private void OnEnable() {

        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable() {

        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent) {

        Debug.Log("Event Called");
        
        if (photonEvent.Code == (byte)RaiseEventsCode.DiedEventCode) {
            
            object[] data = (object[]) photonEvent.CustomData;
            
            string nickNameOfFinishedPlayer = (string) data[0];
            deathOrder = (int) data[1];
            
            GameObject orderUiText = GameManager.instance.finisherTextUi[deathOrder - 1];
            orderUiText.SetActive(true);

            orderUiText.GetComponent<Text>().text = deathOrder + ". " + nickNameOfFinishedPlayer + " Dead";
        };
    }


    public PlayerDeath Evt_Death = new PlayerDeath();

    public int StartingHealth = 100;
    public int CurrentHealth { get; private set; }
    [SerializeField] Image HealthBar;

    public AudioClip deathClip;

    Animator anim;
    AudioSource playerAudio;

    bool isDead;

    void Awake() {

        anim = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();

        ResetPlayer();
    }

    public void ResetPlayer() {

        CurrentHealth = StartingHealth;

        anim.SetBool("IsDead", false);
    }

    [PunRPC]
    public void TakeDamage(int amount) {

        CurrentHealth -= amount;

        HealthBar.fillAmount = (float) CurrentHealth / StartingHealth;

        if (CurrentHealth <= 0 && !isDead) {
            
            Death();
        }

        playerAudio.Play();
    }

    void Death() {

        Debug.Log("DEATH CALLED");

        isDead = true;

        anim.SetBool("IsDead", true);

        deathOrder++;

        string nickName = photonView.Owner.NickName;

        object[] data = new object[] { nickName, deathOrder };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte) RaiseEventsCode.DiedEventCode, data, raiseEventOptions, sendOption);

        playerAudio.clip = deathClip;
        playerAudio.Play();

        Evt_Death.Invoke(this);
    }

    public void RestartLevel() {

        EventManager.TriggerEvent("GameOver");
    }

    [PunRPC]
    void HidePlayer() {

        gameObject.tag = "Untagged";

        GetComponent<PlayerSetup>().SpecatorMode();

        foreach (Transform child in transform) {

            child.gameObject.SetActive(false);
        }
    }


    public void RPCTakeDamage(int damage) {

        GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
    }

    public void RPCHidePlayer() {

        GetComponent<PhotonView>().RPC("HidePlayer", RpcTarget.AllBuffered);
    }
}