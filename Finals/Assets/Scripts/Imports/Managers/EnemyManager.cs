﻿using UnityEngine;
using Photon.Pun;

public class EnemyManager : MonoBehaviourPunCallbacks {
        
    public GameObject enemy;
    public float spawnTime = 5f;
    public Transform[] spawnPoints;

    private float timer;
    private int spawned = 0;

    void Start () {
        timer = spawnTime;
    }

    void Update() {

        timer -= Time.deltaTime;

        if (timer <= 0f) {

            Spawn();
            timer = spawnTime;
        }
    }

    void Spawn () {           

        int spawnPointIndex = Random.Range (0, spawnPoints.Length);
            
        PhotonNetwork.Instantiate(enemy.name, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
}