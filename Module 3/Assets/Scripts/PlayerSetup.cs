﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks {

    public Camera camera { get; private set; }

    public PhotonView PhotonView { get; private set; }
    public VehicleMovement VehicleMovement { get; private set; }
    public LapController LapController { get; private set; }
    public CountdownManager CountdownManager { get; private set; }
    public Shooting Shooting { get; private set; }

    [Header("UI")]
    [SerializeField] Text NameTag;

    void Awake() {

        this.camera = transform.Find("Camera").GetComponent<Camera>();

        PhotonView = GetComponent<PhotonView>();
        VehicleMovement = GetComponent<VehicleMovement>();
        LapController = GetComponent<LapController>();
        CountdownManager = GetComponent<CountdownManager>();
        Shooting = GetComponent<Shooting>();

        CountdownManager.timerText = GameManager.instance.timeText;

        VehicleMovement.enabled = photonView.IsMine;
        LapController.enabled = photonView.IsMine;

        camera.enabled = photonView.IsMine;

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc")) {
            Shooting.enabled = false;
            
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr")) {
            LapController.enabled = false;
        }

        photonView.RPC("UpdateNameTag", RpcTarget.AllBuffered);

        NameTag.gameObject.SetActive(!photonView.IsMine);
    }

    [PunRPC]
    void UpdateNameTag() {
        NameTag.text = PhotonView.Owner.NickName;
    }
}
