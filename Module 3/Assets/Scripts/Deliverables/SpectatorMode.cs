﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpectatorMode : MonoBehaviourPunCallbacks {

    List<Camera> cameras = new List<Camera>();
    int currentView;
    int nextView;

    void Start() {
        foreach (Camera cam in GameManager.instance.Cameras) {
            cameras.Add(cam);
        }

        cameras[0].gameObject.SetActive(true);
    }

    void Update() {
        if (photonView.IsMine) {
            if (Input.GetMouseButtonDown(0)) {
                
                nextView++;
                if (nextView > cameras.Count - 1) {
                    nextView = 0;
                }

                SwitchView();
            }
        }
    }

    void SwitchView() {
        cameras[nextView].gameObject.SetActive(true);

        cameras[currentView].gameObject.SetActive(false);

        currentView = nextView;
    }
}
