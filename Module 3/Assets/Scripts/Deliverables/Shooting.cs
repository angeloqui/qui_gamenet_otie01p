﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class TakeDamage : UnityEvent<Shooting, string, string> {}

public class Shooting : MonoBehaviourPunCallbacks {

    public TakeDamage Evt_TakeDamage = new TakeDamage();

    Camera camera;

    [SerializeField] Image HealthBar;
    [SerializeField] float MaxHealth;
    float currentHealth;
    bool isDead;

    [SerializeField] GameObject bulletPrefab;
    int damage = 10;
    public bool isRaycast;

    public enum RaiseEventsCode {
        WhoDiedEventCode = 0
    }

    private int deathOrder = 0;

    void Start() {
        currentHealth = MaxHealth;
        camera = GetComponent<PlayerSetup>().camera;
    }

    void Update() {
        if (photonView.IsMine && !isDead) {
            if (Input.GetMouseButtonDown(0)) {
                Fire();
            }
        }
    }

    private void OnEnable() {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable() {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    [PunRPC]    
    public void TakeDamage(int value, PhotonMessageInfo info) {

        currentHealth -= value;
        currentHealth = Mathf.Clamp(currentHealth, 0, MaxHealth);

        HealthBar.fillAmount = currentHealth / MaxHealth;

        if (currentHealth <= 0 && !isDead) {

            isDead = true;

            Die();

            gameObject.tag = "Untagged";
            foreach (Transform child in transform) {
                child.gameObject.SetActive(false);
            }

            deathOrder++;

            string nickName = photonView.Owner.NickName;
            int viewId = photonView.ViewID;

            object[] data = new object[] { nickName, deathOrder, viewId };

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };

            SendOptions sendOption = new SendOptions {
                Reliability = false
            };

            PhotonNetwork.RaiseEvent((byte) RaiseEventsCode.WhoDiedEventCode, data, raiseEventOptions, sendOption);

            Evt_TakeDamage.Invoke(this, info.Sender.NickName + " - (X) " + info.photonView.Owner.NickName, info.Sender.NickName);
        }
    }

    void OnEvent(EventData photonEvent) {
        if (photonEvent.Code == (byte)RaiseEventsCode.WhoDiedEventCode) {
            object[] data = (object[]) photonEvent.CustomData;

            string nickNameOfFinishedPlayer = (string) data[0];
            deathOrder = (int) data[1];
            int viewId = (int) data[2];

            GameObject orderUiText = GameManager.instance.finisherTextUi[deathOrder - 1];
            orderUiText.SetActive(true);

            orderUiText.GetComponent<Text>().text = deathOrder + ". " + nickNameOfFinishedPlayer + " Dead";
        }
    }

    void Die() {
        if (!photonView.IsMine) return;

        GetComponent<VehicleMovement>().enabled = false;
        GetComponent<SpectatorMode>().enabled = true;

        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    public void Fire() {

        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.45f));

        if (Physics.Raycast(ray, out hit, 200)) {

            if (!isRaycast) {
                photonView.RPC("CreateBullet", RpcTarget.All);
            } 
            else if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine) {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
            }
        }
    }

    [PunRPC]
    void CreateBullet() {
        GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);

        bullet.transform.position = transform.position;

        Vector3 rotation = bullet.transform.rotation.eulerAngles;

        bullet.transform.rotation = Quaternion.Euler(rotation.x, transform.eulerAngles.y, rotation.z);

        bullet.GetComponent<Rigidbody>().AddForce(transform.forward * 50, ForceMode.Impulse);

        StartCoroutine(DestroyBulletAfterTime(bullet, 4));

        bullet.GetComponent<Projectile>().shooter = this;
    }

    private IEnumerator DestroyBulletAfterTime(GameObject bullet, float delay) {
        yield return new WaitForSeconds(delay);

        Destroy(bullet);
    }

    public void OnProjectileHit(GameObject other) {
        other.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
    }
}
